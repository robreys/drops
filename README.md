# drops #

### Overview ###

Drops is a mobile application that allows users to hide and locate digital content using GPS coordinates. If you are familiar with Geocaching, this is a similar concept. Users can create and share drops (a collection of digital content) at their current location, as well as locate nearby drops from other users.  The application is built using the Ionic framework (AngularJS and Cordova) and uses Parse as a data store.

### Inspiration ###

This app idea came about after learning of USB dead drops and playing the game ‘Infamous’ as a freshman. I later learned about Geocaching and, though it is a similar concept, I had a different motivation for this application. In general, I’d like for users to be able to privately share locations for things like a spare set of keys for when your friend needs to crash at your place. I realize products like Maps already has pin-sharing features, but I wanted an application specifically for sharing of locations. Additionally, I liked the idea of adding an additional layer of virtual content to the location. With this, I figured people would be able to publicly share moments that had occurred at specific locations; for example, a recording of a street performer at a popular location. 

### Thoughts ###

There is still much to think about and a lot to do before this application is ready. I chose the Ionic framework simply because I had no previous experience with mobile app development. I wanted to be able to jump right in with only my web dev experience and Ionic has allowed me to do that.  Because of all the excitement I skipped a crucial part of software development… setting up a testing environment.

It is still early so I have lots of time to get things done the right way. With school a priority, I don’t know how much time I’ll actually be able to put into this, but it is a personal goal of mine to get this finished by the beginning of Spring quarter.