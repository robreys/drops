angular.module('starter')

.service('SessionService', function(User, $state, $ionicHistory) {
  this.checkLoggedIn = function() {
    if (User.current()) {
      return true;
    }
    else {
      return false;
    }
  };

  this.getCurrentUser = function() {
    return User.current();
  };

  this.logout = function() {
    $state.go('login').then(function(current) {
      console.log($ionicHistory.viewHistory());
      $ionicHistory.clearCache();
      $ionicHistory.clearHistory();
      //$ionicNavBarDelegate.showBackButton(false);
      User.logOut();
      console.log($ionicHistory.viewHistory());
    }, function(error) {
      console.log('error in logout', error.message);
    });
  }
});