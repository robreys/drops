angular.module('starter')

.factory('AuthService', function($q, $state, $ionicHistory, $ionicNavBarDelegate, User, SessionService) {
  return {
    signUp: function(raw) {
      var user = new User();
      user.set('username', raw.username);
      user.set("password", raw.password);
      user.set("email", raw.email);

      user.signUp(null, {
        success: function(user) {
          $state.go('tab.find').then();
        },
        error: function(user, error) {
          console.log('error in signup', error.message);
        }
      });
    },
    logIn: function(raw) {
      console.log('raw', raw);
      User.logIn(raw.username, raw.password, {
        success: function(user) {
          $state.go('tab.find').then(function(current) {
            console.log('clearing history and cache');
            $ionicHistory.clearCache();
            $ionicHistory.clearHistory();
            $ionicNavBarDelegate.showBackButton(false);
            console.log($ionicHistory.viewHistory());
          });
        },
        error: function(user, error) {
          console.log('error in login', error.message);
        }
      });
    },
    reset: function(raw) {
      var defer = $q.defer();

      User.requestPasswordReset(raw.email, {
        success: function(user) {
          defer.resolve(user);
        },
        error: function(user, error) {
          defer.reject(error);
        }
      });

      return defer.promise;      
    }
  };
});