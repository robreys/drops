angular.module('starter')

.factory('User', function($q) {
  var properties = [
    'username',
    'friends',
    'cached'
  ];
  var User = Parse.User.extend('User', {
    // instance methods
    addToCached: function(drop) {
      this.addUnique('cached', drop);
      this.save();
    },
    removeFromCached: function(drop) {
      this.remove('cached', drop);
      this.save();
    }
    // TODO: add/remove friends
  }, {
    // class methods
  });  
    // define getters and setters for properties

  User.prototype.__defineGetter__('username', function() {
    return this.get('username');
  });
  User.prototype.__defineSetter__('username', function(value) {
    return this.set('username', value);
  });   
  User.prototype.__defineGetter__('friends', function() {
    return this.get('friends');
  });
  User.prototype.__defineSetter__('friends', function(value) {
    return this.set('friends', value);
  });   
  User.prototype.__defineGetter__('cached', function() {
    return this.get('cached');
  });
  User.prototype.__defineSetter__('cached', function(value) {
    return this.set('cached', value);
  });   
  
  return User;
});