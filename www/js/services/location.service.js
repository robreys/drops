angular.module('starter')

.service('LocationService', function($q) {
  this.position = null;

  // updates and returns current position
  this.updatePosition = function() {
    console.log('updating current position');
    var defer = $q.defer();

    var self = this;
    navigator.geolocation.getCurrentPosition(function (pos) {
      console.log('low accuracy position', pos);
      self.position = pos.coords;
      defer.resolve(self.position);
    }, function (error) {
      // trying again with high accuracy
      navigator.geolocation.getCurrentPosition(function (pos) {
        console.log('high accuracy position', pos);
        self.position = pos.coords;
        defer.resolve(self.position);
      }, function (error) { 
        console.log('Unable to get location: ' + error.message);       
        defer.reject(error);
      }, {
        maximumAge: 0,
        timeout: 15000,
        enableHighAccuracy: true
      });
    }, {
      maximumAge: 0,
      timeout: 10000,
      enableHighAccuracy: false
    });

    return defer.promise;
  }

  this.getPosition = function() {
    return this.position;
  }
});