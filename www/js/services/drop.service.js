angular.module('starter')

.factory('DropService', function($q, SessionService, LocationService) {
  var properties = [
    'owner', // INDEX 0 RESERVED FOR OWNER!
    'title',
    'description',
    'content'
  ];

  // unlocks at about 20 feet
  var UNLOCK_DIST_MILES = 0.003;

  var Drop = Parse.Object.extend('Drop', {
    // instance methods
    addToContent: function(item, skipSave) {
      // view doesnt update using parse convenience method
      // this.add('content', item);

      var content = this.get('content');
      content.push(item);
      // necessary for stripping angular variables with leading $$
      content = angular.fromJson(angular.toJson(content));
      this.set('content', content);
      if (!skipSave)
        return this.triggerSave(null);
    },
    removeFromContent: function(item) {
      // this.remove('content', item);
      var content = this.get('content');
      content.splice(content.indexOf(item), 1);
      // necessary for stripping angular variables with leading $$
      content = angular.fromJson(angular.toJson(content));
      this.set('content', content);
      return this.triggerSave(null);
    },
    updateContentItem: function(index, item) {
      var content = this.get('content');
      content[index] = item;
      // necessary for stripping angular variables with leading $$
      content = angular.fromJson(angular.toJson(content));
      this.set('content', content);
      return this.triggerSave(null);
    },
    triggerSave: function(attr) {
      var defer = $q.defer();
      
      if (attr) {
        attr = angular.fromJson(angular.toJson(attr));
        delete attr.owner;
        delete attr.location;
      }
      this.save(attr, {
        success: function(drop) {
          defer.resolve(drop);
        },
        error: function(drop, error) {
          defer.reject(error);
        }
      });
      return defer.promise;
    },
    triggerDelete: function() {
      var defer = $q.defer();
      this.destroy({
        success: function(drop) {
          defer.resolve(drop);
        },
        error: function(drop, error) {
          defer.reject(error);
        }
      });
      return defer.promise;
    }
  }, {
    createEmpty: function() {
      var drop = new Drop();
      drop.set('content', []);
      return drop;
    },
    // class methods
    create: function(raw) {
      var defer = $q.defer();

      var drop = new Drop();
      // determine current location
      LocationService.updatePosition().then(function(pos) {
        drop.set('location', new Parse.GeoPoint({
          latitude: pos.latitude,
          longitude: pos.longitude
        }));
        drop.set('owner', SessionService.getCurrentUser());
        for (var i = 1; i < properties.length; i++) {
          var rawProp = raw[properties[i]];
          if (angular.isArray(rawProp)) {
            rawProp = angular.fromJson(angular.toJson(rawProp));
          }
          drop.set(properties[i], rawProp);
        }
        // attempt to save
        drop.save(null, {
          success: function(drop) {
            defer.resolve(drop);
          },
          error: function(drop, error) {
            defer.reject(error);
          }
        });
      }, function(error) {
        defer.reject(error);
      }); 

      return defer.promise;
    },

    allByUser: function(user) {
      var defer = $q.defer();

      var query = new Parse.Query(this);
      query.equalTo("owner", user);
      query.find({
        success : function(drops) {
          defer.resolve(drops);
        },
        error : function(error) {
          defer.reject(error);
        }
      });

      return defer.promise;
    },

    allNearMe: function() {
      var defer = $q.defer();

      // determine current location
      LocationService.updatePosition().then(function(pos) {
        console.log('finding near me', pos);
        // User's location
        pos = new Parse.GeoPoint({
          latitude: pos.latitude,
          longitude: pos.longitude
        });
        // Create a query for places
        var query = new Parse.Query(Drop);
        // Interested in locations near user.
        query.near("location", pos);
        // Limit what could be a lot of points.
        query.limit(10);
        // Final list of objects
        query.find({
          success: function(drops) {
            for (var i = 0; i < drops.length; i++) {
              var drop = drops[i];
              drop.milesTo = drop.location.milesTo(pos).toFixed(3);
              if (drop.milesTo <= UNLOCK_DIST_MILES) {
                drop.unlocked = true;
              }
            }
            defer.resolve(drops);
          },
          error: function(error) {
            defer.reject(error);
          }
        });
      }, function(error) {
        defer.reject(error);
      }); 

      return defer.promise;
    }
  });

  // define getters and setters for properties
  Drop.prototype.__defineGetter__('owner', function() {
    return this.get('owner');
  });
  Drop.prototype.__defineSetter__('owner', function(value) {
    return this.set('owner', value);
  });   
  Drop.prototype.__defineGetter__('title', function() {
    return this.get('title');
  });
  Drop.prototype.__defineSetter__('title', function(value) {
    return this.set('title', value);
  }); 
  Drop.prototype.__defineGetter__('description', function() {
    return this.get('description');
  });
  Drop.prototype.__defineSetter__('description', function(value) {
    return this.set('description', value);
  }); 
  Drop.prototype.__defineGetter__('content', function() {
    return this.get('content');
  });
  Drop.prototype.__defineSetter__('content', function(value) {
    return this.set('content', value);
  }); 
  Drop.prototype.__defineGetter__('location', function() {
    return this.get('location');
  });
  Drop.prototype.__defineSetter__('location', function(value) {
    return this.set('location', value);
  }); 
  // Drop.prototype.__defineGetter__('milesTo', function() {
  //   return this.get('milesTo');
  // });
  // Drop.prototype.__defineSetter__('milesTo', function(value) {
  //   return this.set('milesTo', value);
  // }); 
  

  return Drop;
});