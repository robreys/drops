angular.module('starter')
.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
  .state('signup', {
    url: '/signup',
    templateUrl: 'templates/signup.html',
    controller: 'SignUpCtrl'
  })
  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'LogInCtrl'
  })
  .state('reset', {
    url: '/reset',
    templateUrl: 'templates/reset.html',
    controller: 'ResetCtrl'
  })

  // CREATE A NEW DROP STATE
  .state('create', {
    url: '/create',
    cache: false,
    resolve: {
      belongsToCurrentUser: function() {
        // we want to see the save button
        return true;
      },
      drop: function(DropService) {
        return DropService.createEmpty();
      },
      initMode: function() {
        return 'CREATE';
      }
    },
    templateUrl: 'templates/details/drop-detail.html',
    controller: 'DropDetailCtrl'
  })
  .state('create.content', {
    url: '/content?i',
    resolve: {
      item: function($stateParams, drop) {
        if (!$stateParams.i) {
          return null;
        }
        else {
          return {
            i: $stateParams.i,
            val: drop.content[$stateParams.i]
          };
        }
      }
    },
    views: {
      '@': {
        templateUrl: 'templates/details/content-detail.html',
        controller: 'ContentItemDetailCtrl'
      }
    }
  })
  // setup an abstract state for the tabs directive
  .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })
  // Each tab has its own nav history stack:

  // SHOW YOUR PROFILE AND ACCOUNT SETTINGS TAB
  .state('tab.me', {
    url: '/me',
    resolve: {
      user: function(SessionService) {
        return SessionService.getCurrentUser();
      },
      drops: function(DropService, user) {
        return DropService.allByUser(user);
      }
    },
    views: {
      'tab-me': {
        templateUrl: 'templates/details/user-detail.html',
        controller: 'CurrentUserDetailCtrl'
      }
    }
  })
  .state('tab.me.detail', {
    url: '/:dropId',
    resolve: {
      belongsToCurrentUser: function() {
        return true;
      },
      drop: function($stateParams, drops) {
        for (var i = 0; i < drops.length; i++) {
          if (drops[i].id == $stateParams.dropId) {
            return drops[i];
          }
        }
      },
      initMode: function() {
        return 'DETAIL';
      }
    },   
    views: {
      'tab-me@tab': {
        templateUrl: 'templates/details/drop-detail.html',
        controller: 'DropDetailCtrl'
      }
    }
  })
  .state('tab.me.detail.content', {
    url: '/content?i',
    resolve: {
      item: function($stateParams, drop) {
        if (!$stateParams.i) {
          return null;
        }
        else {
          return {
            i: $stateParams.i,
            val: drop.content[$stateParams.i]
          };
        }
      }
    },
    views: {
      'tab-me@tab': {
        templateUrl: 'templates/details/content-detail.html',
        controller: 'ContentItemDetailCtrl'
      }
    }
  })

  // FIND NEARBY DROPS TAB
  .state('tab.find', {
    url: '/find',
    resolve: {
      drops: function(DropService) {
        console.log('trying to resolve');
        return DropService.allNearMe();
      }
    },
    views: {
      'tab-find': {
        templateUrl: 'templates/tabs/tab-find.html',
        controller: 'FindCtrl'
      }
    }
  })

  .state('tab.find.detail', {
    url: '/:dropId',
    resolve: {
      drop: function($stateParams, drops) {
        for (var i = 0; i < drops.length; i++) {
          if (drops[i].id == $stateParams.dropId) {
            return drops[i];
          }
        }
      }
    },
    views: {
      'tab-find@tab': {
        templateUrl: 'templates/details/find-detail.html',
        controller: 'MapCtrl'
      }
    }
  })

  // LIST YOUR CACHED DROPS TAB
  .state('tab.cached', {
    url: '/cached', 
    views: {
      'tab-cached': {
        templateUrl: 'templates/tabs/tab-cached.html',
        controller: 'CachedCtrl'
      }
    }
  })
  .state('tab.cached.detail', {
    url: '/:dropId',
    resolve: {
      belongsToCurrentUser: function() {
        return false;
      },
      drop: function($stateParams, DropService) {
        // return Drops.get($stateParams.dropId)
        return DropService.get($stateParams.dropId);
      },
      initMode: function() {
        return 'DETAIL';
      }
    },  
    views: {
      'tab-cached@tab': {
        templateUrl: 'templates/details/drop-detail.html',
        controller: 'DropDetailCtrl'
      }
    }
  }) 
  .state('tab.cached.detail.content', {
    url: '/content?i',
    resolve: {
      item: function($stateParams, drop) {
        return {
          i: $stateParams.i,
          val: drop.content[$stateParams.i]
        }
      }
    },
    views: {
      'tab-cached@tab': {
        templateUrl: 'templates/details/content-detail.html',
        controller: 'ContentItemDetailCtrl'
      }
    }
  })
;

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

});