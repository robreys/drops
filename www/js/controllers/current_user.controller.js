angular.module('starter')
.controller('CurrentUserDetailCtrl', function($scope, $state, $ionicPopup, user, drops, SessionService) {
  $scope.isCurrentUser = true;

  $scope.user = user;
  $scope.user.drops = drops;

  $scope.logout = function() {
    //$scope.$on('$ionicView.afterLeave', function() {
      SessionService.logout();
    //});
    //$state.go('login');
  }

  $scope.deleteDrop = function(drop) {
    $ionicPopup.confirm({
      title: 'Delete Drop',
      template: 'Permanently delete this?'
    }).then(function(res) {
      if(res) {
        drop.triggerDelete().then(function(del) {
          $scope.user.drops.splice($scope.user.drops.indexOf(drop), 1);
        }, function(error) {
          console.log('error in drop delete', error.message);
        });
      }
    });
  }
});