angular.module('starter')
.controller('SignUpCtrl', function($scope, $ionicLoading, AuthService) {
  $scope.user = {};
  // TODO: loading icon
  $scope.signup = function() {
    $ionicLoading.show({hideOnStateChange: true});
    //TODO: validate fields
    AuthService.signUp($scope.user);
  };
})
.controller('ResetCtrl', function($scope, $state, $ionicPopup, AuthService) {
  $scope.user = {};
  $scope.send = function() {
    // TODO: Validate email address
    // TODO: Create html template. "email has been sent to {email}"
    $ionicPopup.alert({
       title: 'Success!',
       template: 'An email has been sent.' 
    }).then(function() {
      $state.go('login');
    });
  };
});