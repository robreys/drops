angular.module('starter')
.controller('LogInCtrl', function($scope, $state, $ionicLoading, AuthService, SessionService) {
  // TODO: check if user is already logged in
  // maybe we want a seperate 'home' state to display logo for a bit
  // and then transition to login if user is not already logged in
  $ionicLoading.show({hideOnStateChange: true});

  if (SessionService.checkLoggedIn()) {
    $state.go('tab.find');
  }
  else {
    $ionicLoading.hide();
    $scope.user = {
      username: "RoqRobster",
      password: "123reyes"
    };
  }
  
  $scope.login = function() {
    $ionicLoading.show({hideOnStateChange: true});
    //TODO: validate fields
    console.log($scope);
    AuthService.logIn($scope.user);
  };
});