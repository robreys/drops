angular.module('starter')
.controller('FindCtrl', function($scope, drops, $ionicLoading, DropService) {
  $ionicLoading.hide();
  $scope.drops = drops;

  $scope.doRefresh = function() {
    DropService.allNearMe().then(function(drops) {
      $scope.drops = drops;
    },
    function(error) {
      console.log('error in find', error.message);
    }).finally(function() {
      $scope.$broadcast('scroll.refreshComplete');
    });
  }
});