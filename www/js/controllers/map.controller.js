angular.module('starter')
.controller('MapCtrl', function($scope, $ionicLoading, drop, LocationService) {
  $scope.drop = drop;

  $scope.mapCreated = function(map) {
    $scope.map = map;
    $ionicLoading.show();

    var pos = LocationService.getPosition();
    // user location
    var myPos = new google.maps.LatLng(pos.latitude, pos.longitude);
    // drop location
    var dropLoc = $scope.drop.location;
    var dropPos = new google.maps.LatLng(dropLoc.latitude, dropLoc.longitude);  

    console.log('myPos', pos);
    console.log('dropPos', dropLoc);

    $scope.map.fitBounds(new google.maps.LatLngBounds(myPos, dropPos));
    // add markers for user and drop locations
    addMarker(myPos, 'O');
    addMarker(dropPos, 'D');

    $ionicLoading.hide();

    function addMarker(pos, label) {
      new google.maps.Marker({
        position: pos,
        map: $scope.map,
        label: label
      });
    }
    
  };
});