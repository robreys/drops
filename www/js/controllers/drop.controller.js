angular.module('starter')
.controller('DropDetailCtrl', function($scope, $ionicLoading, $state, DropService,
                            belongsToCurrentUser, drop, initMode) {

  $scope.belongsToCurrentUser = belongsToCurrentUser;
  $scope.drop = drop;
  $scope.mode = initMode;

  if (initMode == 'CREATE') {
    $scope.modify = true;
    $scope.title = "Drop Creation";
  }
  else { // DETAIL
    $scope.modify = false;
    $scope.title = drop.title;
  }

  $scope.detail = function() {
    $scope.mode = 'DETAIL';
    $scope.modify = false;
    $scope.title = $scope.drop.title;
  };

  $scope.edit = function() {  
    // deep copy to prevent writing before clicking save
    var temp = $scope.drop.clone();
    $scope.drop = temp;

    $scope.mode = 'EDIT';
    $scope.modify = true;
    $scope.title = '(Edit) ' + $scope.title;
  };

  $scope.save = function() {
    // loading icon
    $ionicLoading.show();
    if ($scope.mode == 'CREATE') {
      console.log($scope.drop.attributes);
      DropService.create($scope.drop.attributes).then(function(drop) {
        $ionicLoading.hide();
        $scope.drop = drop;
        $scope.detail();
        $scope.fresh = true;
      }, function(error) {
        console.log('error creating', error.message);
      });
    }
    else { // EDIT MODE
      // Drops.update($scope.drop.dropId, $scope.drop);
      drop.triggerSave($scope.drop.attributes).then(function(updated) {
        $ionicLoading.hide();
        $scope.drop = updated;
        $scope.detail();
      }, function(error) {
        console.log('error saving', error.message);
      });
    }
  };

  $scope.remove = function(item) {
    drop.removeFromContent(item).then(function(drop) {
      $scope.drop = drop;
    }, function(error) {
      console.log('error removing content-item', error.message);
    })
    // $scope.drop.content.splice($scope.drop.content.indexOf(item), 1);
  };

  $scope.showContent = function(item) {
    $state.go('.content', {i: $scope.drop.content.indexOf(item)});
  };
});