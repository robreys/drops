angular.module('starter')
.controller('ResetCtrl', function($scope, $state, $ionicPopup, AuthService) {
  $scope.user = {};
  $scope.send = function() {
    // TODO: Validate email address
    // TODO: Create html template. "email has been sent to {email}"
    $ionicPopup.alert({
       title: 'Success!',
       template: 'An email has been sent.' 
    }).then(function() {
      $state.go('login');
    });
  };
});