angular.module('starter')
.controller('ContentItemDetailCtrl', function($scope, $ionicHistory, $ionicLoading, belongsToCurrentUser, drop, item) {
  $scope.belongsToCurrentUser = belongsToCurrentUser;

  if (item) {
    $scope.item = item.val;
    $scope.mode = 'DETAIL';
    $scope.modify = false;
    $scope.title = 'View Content';
  }
  else {
    $scope.item = {};
    $scope.mode = 'CREATE'
    $scope.modify = true;
    $scope.title = 'New Content';
  } 

  $scope.edit = function() {
    // deep copy to prevent writing before clicking 'done'
    var temp = {};
    angular.copy($scope.item, temp);
    $scope.item = temp;

    $scope.mode ='EDIT';
    $scope.modify = true;
    $scope.title = 'Edit Content';
  }

  $scope.done = function() {
    $ionicLoading.show();
    if ($scope.mode == 'EDIT') {
      drop.updateContentItem(item.i, $scope.item).then(function(drop) {
        $ionicLoading.hide();
        $ionicHistory.goBack();
      }, function(error) {
        console.log("error editing content", error.message);
      });
    }
    else { // CREATE
      console.log(drop);
      if (drop.id) {
        drop.addToContent($scope.item).then(function(drop) {
          $ionicLoading.hide();
          $ionicHistory.goBack();
        }, function(error) {
          console.log("error creating content", error.message);
        });
      }
      else {
        drop.addToContent($scope.item, true);
        $ionicLoading.hide();
        $ionicHistory.goBack();
      }
    }
  }
});