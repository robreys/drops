angular.module('starter')

.filter('approx', function() {
  return function(dist) {
    // input = input || '';
    dist = parseFloat(dist);
    if (dist < 1) {
      return (dist * 5280).toFixed(2) + " feet away.";
    }
    else {
      return (dist.toFixed(2)) + " miles away.";
    }
  };
});
// .filter('time', function(time) {

// });